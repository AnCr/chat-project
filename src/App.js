import "./App.css";
import { useEffect } from "react";
import ChatContainer from "./components/ChatContainer"; //ChatContainer потому что default
// import io from "socket.io-client";

function App() {
  return (
    <div className="App">
      <ChatContainer />
    </div>
  );
}

export default App;
