import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { useEffect } from "react";
import {
  getDialogs,
  getDialogsCount,
  setActiveDialogId,
} from "../redux/dialogs/actionCreators";
import {
  dialogsSelector,
  dialogsStatusSelector,
  dialogsCountSelector,
  activeDialogIdSelector,
} from "../redux/dialogs/selectors";
import { PROMISE_STATUSES } from "../redux/constants";
import { Dialog } from "./Dialog";

/*
  2. создать компонент dialog
  3. нам нужно сетать id активного диалога в общее хранилище
  4. в компоненте MessagesContainer вытаскивать id активного чата и запрашивать сообщения для него
  5. react virtualized и подгрузка диалогов по limit, skip
*/

const mapStateToProps = (store) => {
  return {
    dialogs: dialogsSelector(store),
    status: dialogsStatusSelector(store),
    count: dialogsCountSelector(store),
    activeDialogId: activeDialogIdSelector(store),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDialogs: bindActionCreators(getDialogs, dispatch),
    getDialogsCount: bindActionCreators(getDialogsCount, dispatch),
    setActiveDialogId: bindActionCreators(setActiveDialogId, dispatch), // диспатчеры
  };
};

export const DialogsContainer = ({
  dialogs,
  status,
  count,
  activeDialogId,
  getDialogs,
  getDialogsCount,
  setActiveDialogId,
}) => {
  useEffect(() => {
    getDialogs();
    getDialogsCount();
  }, []);

  const handleDialogClick = (id) => setActiveDialogId(id);

  if (status === PROMISE_STATUSES.PENDING) {
    return (
      <div className="DialogsContainer">
        <div>загрузка</div>
      </div>
    );
  }

  return (
    <div className="DialogsContainer">
      {dialogs.map((dialog) => {
        const { messages } = dialog;

        // console.log("messages", messages);
        const [firstMessage = {}] = messages || [];
        return (
          <Dialog
            key={dialog._id}
            message={firstMessage.text}
            timestamp={firstMessage.createdAt}
            id={dialog._id}
            activeDialogId={activeDialogId}
            onDialogClick={handleDialogClick}
          />
        );
        //return <div>{firstMessage.text}</div>;
      })}
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogsContainer);
