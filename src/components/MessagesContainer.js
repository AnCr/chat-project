import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { activeDialogIdSelector } from "../redux/dialogs/selectors";
import { useEffect, useState } from "react";
import { getMessages, sendMessage } from "../redux/messages/actionCreators";
import { messagesSelector } from "../redux/messages/selectors";

const mapStateToProps = (store) => {
  // ни в коем случае здесь ничего не пиши
  return {
    activeDialogId: activeDialogIdSelector(store),
    messages: messagesSelector(store),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMessages: bindActionCreators(getMessages, dispatch),
    sendMessage: bindActionCreators(sendMessage, dispatch),
  };
};

const MessagesContainer = ({
  messages,
  activeDialogId,
  getMessages,
  sendMessage,
}) => {
  const [message, setMessage] = useState("");

  useEffect(() => {
    getMessages({ id: activeDialogId });
  }, [activeDialogId]);

  const handleChange = ({ target }) => {
    const { value } = target;

    setMessage(value);
  };

  const handleClick = () => {
    if (!message) {
      return;
    }
    sendMessage({ id: activeDialogId, text: message });
  };

  // console.log("messages", messages);

  return (
    <div>
      <div className="MessagesContainer">
        {messages.map((message, index) => {
          return <div key={index}>{message.text}</div>;
        })}
      </div>
      <div>
        <input onChange={handleChange} />
        <button onClick={handleClick}>Send</button>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesContainer);
