import React, { useState } from "react";
import { FixedSizeList as List } from "react-window";
import InfiniteLoader from "react-window-infinite-loader";
import AutoSizer from "react-virtualized-auto-sizer";

const LOADING = 1;
const LOADED = 2;

export default function Dialogs2() {
  const [itemStatusMap, setItemStatusMap] = useState([]);

  if (itemStatusMap.length === 0) {
    let count = 0;
    const newData = [];
    while (count < 500 + Math.round(Math.random() * 500)) {
      newData.push(0);
      count++;
    }
    setItemStatusMap(newData);
  }

  const isItemLoaded = (index) => itemStatusMap[index] === LOADED;
  const loadMoreItems = (startIndex, stopIndex) => {
    console.log({ startIndex, stopIndex });
    const nState = itemStatusMap.concat();
    for (let index = startIndex; index <= stopIndex; index++) {
      nState[index] = LOADING;
    }
    setItemStatusMap(nState);
    return new Promise((resolve) =>
      setTimeout(() => {
        const nState = itemStatusMap.concat();
        for (let index = startIndex; index <= stopIndex; index++) {
          nState[index] = LOADED;
          setItemStatusMap(nState);
        }
        resolve();
      }, 2500)
    );
  };

  const ItemRenderer = ({ style, index }) => {
    if (itemStatusMap[index] === LOADED) {
      return (
        <div className="ListItem" style={style}>
          {`Row ${index}`}
        </div>
      );
    }
    return (
      <div className="ListItem" style={style}>
        {"Loading..."}
      </div>
    );
  };

  return (
    <div>
      <p className="Note">
        This demo app mimics loading and functional component aproach
      </p>
      <div className="ContainerList">
        <AutoSizer>
          {({ width, height }) => (
            <InfiniteLoader
              isItemLoaded={isItemLoaded}
              itemCount={itemStatusMap.length}
              loadMoreItems={loadMoreItems}
            >
              {({ onItemsRendered, ref }) => (
                <List
                  className="List"
                  height={height}
                  itemCount={itemStatusMap.length}
                  itemSize={50}
                  onItemsRendered={onItemsRendered}
                  ref={ref}
                  width={width}
                >
                  {ItemRenderer}
                </List>
              )}
            </InfiniteLoader>
          )}
        </AutoSizer>
      </div>
    </div>
  );
}
