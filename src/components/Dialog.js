import classNames from "classnames";
import { getDate } from "../lib/helpers";

export const Dialog = ({
  id,
  message,
  timestamp,
  activeDialogId,
  onDialogClick,
}) => {
  return (
    <div
      className={classNames("Dialog", { Dialog_active: id === activeDialogId })}
      onClick={() => onDialogClick(id)}
    >
      <div className="Dialog__Avatar">Pic</div>
      <div className="Dialog__Description">
        <h3>Nick</h3>
        <p>{message}</p>
      </div>
      <div className="Dialog__Status">
        <span>{getDate(timestamp)}</span>
        <span>Read?</span>
      </div>
    </div>
  );
};
