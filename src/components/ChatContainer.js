import { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getChats, getMessages } from "../lib";
import { actionFullLogin } from "../redux/auth/actionCreators";
import DialogsContainer from "./DialogsContainer";
import MessagesContainer from "./MessagesContainer";
import { socketDispatch } from "../redux/socket/actionCreators";
//import Dialogs from "./Dialogs2";
import { Dialog3 } from "./Dialog3";

const mapStateToProps = (store) => {
  return {
    dialogs: store.dialogs,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actionFullLogin: bindActionCreators(actionFullLogin, dispatch),
    socketDispatch: bindActionCreators(socketDispatch, dispatch),
    // прочитать по bindActionCreators
  };
};

const ChatContainer = ({ actionFullLogin, socketDispatch }) => {
  useEffect(() => {
    actionFullLogin();

    // получать не только текст в сообщений, но и id чье сообщение
    const socket = window.io("ws://chat.fs.a-level.com.ua");

    if (localStorage.authToken) {
      socket.emit("jwt", localStorage.authToken);
    }

    //socket.on("jwt_ok", (data) => console.log(data));
    // socket.on("jwt_fail", (error) => console.log(error));
    socket.on("msg", (msg) => console.log("msg", msg));

    socket.on("chat", (chat) => {
      console.log("chat", chat);
      socketDispatch(chat);
    });
  }, []);

  return (
    <div className="ChatContainer">
      <Dialog3 />
      {/* <DialogsContainer /> */}
      <MessagesContainer />
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatContainer);
