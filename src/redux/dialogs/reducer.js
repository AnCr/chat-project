import {
  DIALOGS_SUCCESS,
  DIALOGS_COUNT_SUCCESS,
  DIALOGS_ACTIVE_ID,
} from "../constants";

const initialState = {
  data: [],
  count: null,
  activeDialogId: null,
};

export const dialogsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case DIALOGS_SUCCESS: {
      return { ...state, data: payload };
    }
    case DIALOGS_COUNT_SUCCESS: {
      return { ...state, count: payload };
    }
    case DIALOGS_ACTIVE_ID: {
      return { ...state, activeDialogId: payload };
    }
  }

  return state;
};
