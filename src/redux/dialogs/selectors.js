import { PROMISE_STATUSES } from "../constants";

export const dialogsSelector = ({ dialogs }) => dialogs.data;

export const dialogsStatusSelector = ({ promise }) =>
  (promise.dialogs && promise.dialogs.status) || PROMISE_STATUSES.PENDING;

export const dialogsCountSelector = ({ dialogs }) => dialogs.count;

export const activeDialogIdSelector = ({ dialogs }) => dialogs.activeDialogId;
