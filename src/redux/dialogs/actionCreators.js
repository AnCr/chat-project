import { actionPromise } from "../actionPromise";
import { gql } from "../../lib";
import {
  DIALOGS_SUCCESS,
  DIALOGS_COUNT_SUCCESS,
  DIALOGS_ACTIVE_ID,
} from "../constants";

const dialogsActionCreator = (payload) => ({ type: DIALOGS_SUCCESS, payload }); // было data -> payload так принято

const dialogsAction = () =>
  actionPromise(
    "dialogs",
    gql(
      `
      query chats($query:String) {
        ChatFind(query:$query) {
          owner{
            _id
          },
          _id,
          title,
          messages {
             text,
              createdAt
           },
          members {
             _id nick
           }
        }
      }`,
      {
        query: JSON.stringify([
          { "members._id": "61e9ccfdbdc4cc1a5f301ec4" },
          { limit: [83] },
        ]),
      }
    )
  );

export const getDialogs = () => async (dispatch) => {
  const data = await dispatch(dialogsAction());

  if (data) {
    dispatch(dialogsActionCreator(data));
  }

  return data;
};

//--------------------------------------------------------------------------------------------------------------------------
const dialogCountActionCreator = (payload) => ({
  type: DIALOGS_COUNT_SUCCESS,
  payload,
});

export const fetchDialogs = () => {
  return gql(
    `
  query count($query:String){
    ChatCount(query:$query)
  }`,
    {
      query: JSON.stringify([
        // { "members._id": "61e9ccfdbdc4cc1a5f301ec4" },
        {},
      ]),
    }
  );
};

const dialogCountAction = () =>
  actionPromise(
    "count",
    gql(
      `
    query count($query:String){
      ChatCount(query:$query)
    }`,
      {
        query: JSON.stringify([
          { "members._id": "61e9ccfdbdc4cc1a5f301ec4" },
          {},
        ]),
      }
    )
  );

export const getDialogsCount = () => async (dispatch) => {
  const data = await dispatch(dialogCountAction());

  if (data) {
    dispatch(dialogCountActionCreator(data));
  }

  return data;
};

/* ==========  */
const activeDialogIdActionCreator = (payload) => ({
  type: DIALOGS_ACTIVE_ID,
  payload,
});

export const setActiveDialogId = (id) => (dispatch) => {
  dispatch(activeDialogIdActionCreator(id));
};
