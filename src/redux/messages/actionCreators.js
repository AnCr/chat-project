import { actionPromise } from "../actionPromise";
import { gql } from "../../lib";
import { MESSAGES_SUCCESS, SEND_MESSAGE_SUCCESS } from "../constants";

const messagesActionCreator = (payload) => ({
  type: MESSAGES_SUCCESS,
  payload,
});

const messagesAction = ({ id }) => {
  return actionPromise(
    "messages",
    gql(
      `
   query messages($query:String){
       MessageFind(query:$query){          
             _id text owner{_id}
       }
   }`,
      {
        query: JSON.stringify([{ "chat._id": id }, { limit: [100] }]),
      }
    )
  );
};

export const getMessages =
  ({ id }) =>
  async (dispatch) => {
    const data = await dispatch(messagesAction({ id }));

    if (data) {
      dispatch(messagesActionCreator(data));
    }

    return data;
  };

//---------------------------------------------------------------

const sendMessageActionCreator = (payload) => ({
  type: SEND_MESSAGE_SUCCESS,
  payload,
});

const sendMessageAction = ({ id, text }) => {
  return actionPromise(
    "messages",
    gql(
      `
        mutation sendMessage($text:String,$id:ID){
          MessageUpsert(message:{text: $text, chat: 
            { _id : $id}}){
            _id
          }
        }
      `,
      {
        text,
        id,
      }
    )
  );
};

export const sendMessage =
  ({ id, text }) =>
  async (dispatch) => {
    const data = await dispatch(sendMessageAction({ id, text }));

    if (data) {
      dispatch(sendMessageActionCreator(data));
    }

    return data;
  };

//--------------------------------------------------------------
const changeMessageAction = () => {
  return actionPromise(
    "messages",
    gql(
      `
          mutation sendMessage($idMsg:ID, $text:String,$id:ID){
            MessageUpsert(message:{_id:$idMsg, text: $text, chat: 
              { _id : $id}}){
              _id
            }
          }
        `,
      {
        text: "changed",
        id: "5e1cad6678317a571f3b3aac",
        idMsg: "61e475d13db2ee2f4fa05387",
      }
    )
  );
};

// setTimeout(() => {
//   console.log("before");
//   changeMessageAction();
//   console.log("after");
// }, 5000);

const createChatAction = () => {
  return actionPromise(
    "newChat",
    gql(
      `
          mutation createChat( $member2:ID){
            ChatUpsert(chat:{ members: 
              [{_id:$member2}]}){
                _id
              }     
          }
        `,
      {
        // member1: "61e9ccfdbdc4cc1a5f301ec4",
        member2: "61e9ccbfbdc4cc1a5f301ec3",
      }
    )
  );
};
//$member1:ID,
//{_id:$member1},
//createChatAction();

const deleteChatAction = () => {
  return actionPromise(
    "deleteChat",
    gql(
      `
          mutation deleteChat( $id:ID){
            ChatDelete(chat: {_id: $id}){
                _id
              }     
          }
        `,
      {
        id: "61e9da60bdc4cc1a5f301eca",
      }
    )
  );
};
//deleteChatAction();

const sendMessageActionTest = () => {
  return actionPromise(
    "messages",
    gql(
      `
        mutation sendMessage($text:String,$id:ID){
          MessageUpsert(message:{text: $text, chat: 
            { _id : $id}}){
            _id
          }
        }
      `,
      {
        text: "how is it going?",
        id: "61e9c9a8bdc4cc1a5f301eb7", // id чата с Даней
      }
    )
  );
};

// setTimeout(() => {
//   sendMessageActionTest();
// }, 3000);

//sendMessageActionTest();

//PerfectDan 61e9ccbfbdc4cc1a5f301ec3
//Ani 61e9ccfdbdc4cc1a5f301ec4
