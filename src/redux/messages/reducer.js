import { MESSAGES_SUCCESS } from "../constants";
import { SEND_MESSAGE_SUCCESS } from "../constants";

const initialState = {
  data: [],
};

export const messagesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case MESSAGES_SUCCESS: {
      return { ...state, data: payload };
    }
    case SEND_MESSAGE_SUCCESS: {
      return { ...state };
    }
  }

  return state;
};
