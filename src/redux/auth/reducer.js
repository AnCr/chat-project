export const authReducer = (state, { type, token }) => {
  // console.log("state:", state);
  // console.log("type, token:", type, token);

  if (!state) {
    if (localStorage.authToken) {
      type = "AUTH_LOGIN";
      token = localStorage.authToken;
    } else {
      return {};
    }
  }
  if (type === "AUTH_LOGIN") {
    // let auth = jwtDecode(token);
    let auth = token;

    if (auth) {
      localStorage.authToken = token;
      return { token, payload: auth };
    }
  }
  if (type === "AUTH_LOGOUT") {
    localStorage.authToken = "";
    return {};
  }
  return state;
};
