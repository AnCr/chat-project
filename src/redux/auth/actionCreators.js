import { actionPromise } from "../actionPromise";
import { gql } from "../../lib";

const actionAuthLogin = (token) => ({ type: "AUTH_LOGIN", token });

const actionLogin = (login, password) =>
  actionPromise(
    "login",
    gql(
      `query ($login:String, $password:String){ 
         login(login:$login, password:$password)}`,
      { login: login, password: password }
    )
  );

// dispatcher
export const actionFullLogin =  //переименовать в фетч/гет токен
  (login = "Ani", password = "123") =>
  async (dispatch) => {
    let token = await dispatch(actionLogin(login, password));

    if (token) {
      dispatch(actionAuthLogin(token));
    }
  };

const actionRegister = (
  login = "AniTest",
  password = "123",
  nick = "AniTest"
) =>
  actionPromise(
    "register",
    gql(
      `
    mutation reg($user:UserInput) {

       UserUpsert(user:$user) {
       _id 
       }
    }
    `,
      { user: { login, password, nick } }
    )
  );
actionRegister();
