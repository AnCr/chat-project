import { SOCKET_JWT_OK } from "../constants";
import { SOCKET_JWT_FAIL } from "../constants";
import { SOCKET_MESSAGE } from "../constants";
import { SOCKET_CHAT } from "../constants";

const initialState = {
  data: [],
};

export const socketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SOCKET_JWT_OK: {
      return { ...state, data: payload };
    }
    case SOCKET_JWT_FAIL: {
      return { ...state, data: payload };
    }
    case SOCKET_MESSAGE: {
      return { ...state, data: payload };
    }
    case SOCKET_CHAT: {
      return { ...state, data: payload };
    }
  }

  return state;
};
