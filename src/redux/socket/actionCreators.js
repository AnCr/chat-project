import { SOCKET_CHAT } from "../constants";

const socketActionCreator = (payload) => ({
  type: SOCKET_CHAT,
  payload,
});

export const socketDispatch = (payload) => (dispatch) => {
  dispatch(socketActionCreator(payload));
};
