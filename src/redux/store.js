import thunk from "redux-thunk";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { authReducer } from "./auth/reducer";
import { dialogsReducer } from "./dialogs/reducer";
import { promiseReducer } from "./actionPromise";
import { messagesReducer } from "./messages/reducer";
import { socketReducer } from "./socket/reducer";

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

export const store = createStore(
  combineReducers({
    auth: authReducer,
    dialogs: dialogsReducer,
    promise: promiseReducer,
    messages: messagesReducer,
    socket: socketReducer,
  }),
  enhancer
);
