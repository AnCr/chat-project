const getGQL =
  (url) =>
  (query, variables = {}) =>
    fetch(url, {
      //метод
      method: "POST",
      headers: {
        //заголовок content-type
        "Content-Type": "application/json",
        ...(localStorage.authToken
          ? { Authorization: "Bearer " + localStorage.authToken }
          : {}),
      },
      //body с ключами query и variables
      body: JSON.stringify({ query, variables }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data.data && data.errors) {
          throw new Error(JSON.stringify(data.errors));
        }
        return data.data[Object.keys(data.data)[0]];
        //расковырять data, если все ок - отдать data.login или data.CategoryFindOne, или шо там еще
        //если есть errors и нет data, то выбросить исключение и тем самым зареджектить промис
      })
      .catch((error) => console.log("error", error));

export const gql = getGQL("http://chat.fs.a-level.com.ua/graphql");

export const getToken = async () => {
  const token = await gql(
    `
     query name($login:String, $password:String){
         login(login:$login, password:$password)
     }`,
    { login: "Ana", password: "123" }
  );

  localStorage.authToken = token;

  return token;
};

/*
  Interface Member = {
    _id: string;
    nick: string;
  };

  Interface Message = {
    _id: string;
    text: string;
  };

  Interface Chat = {
    _id: string;
    members: []Member
    messages: []Message
    owner: {}
    title: null
  }
*/

export const getChats = async () => {
  const result = await gql(
    `
    query chats($query:String) {
      ChatFind(query:$query) {
        messages {
           text
         },
        members {
           nick
         }
      }
    }`,
    {
      query: JSON.stringify([{}, { limit: [20] }]),
    }
  );

  // console.log(result);
};

export const getMessages = async () => {
  let _id = "5e1ca97b78317a571f3b3aaa";
  const result = await gql(
    `
     query chats($_id:String){
         ChatFindOne(query:$_id){          
            _id 
            messages{
               text
            }
         }
     }`,
    { _id: JSON.stringify([{ _id }]) }
  );
  // console.log(result);
};

export default {};
