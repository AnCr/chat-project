export const getDate = (timestamp) => {
  const time = new Date(+timestamp);

  return `${time.getHours()}:${time.getMinutes()}`;
};
